# HPC Projet 1
### Tommy Gerardi, HEIG-VD 2020
## 1
### Software descritpion
The software I chose is a small matrix library coded in C and called *yet another simple Matrix Library - (yasML)*. It's purpose is to provide a set of simple and fast functions to manipulate matrices.
### Motivations
I mainly chose to use this library because I didn't find anything else that I felt like optimizing at the time. Other than that, this one made me realize that I have completely forgotten most of the basic operations one can apply to a matrix, making it a great occasion to refresh my memory.
### Installation procedure
The installation in pretty simple, all you have to do is clone the following repo : https://github.com/nhomble/yasML  
It contains the library as a C header file which contains all of the functions (*yasML.h*). There is also a *tests* folder containing some small examples which use unity to do some unit testing. The files incuded by default in this folder are *identity.c* and *multiply.c*.
The programms which use this library therefore only need to include *yasML.h*.   
My repo, which contains my modifications, can be found here : https://gitlab.com/Thorkal/hcp_pro_tg
## 2
### Benchmark strategy
To benchmark this library, using the default test programms included will not suffice, they are too poor and do not use enough of the library, thus I've decided to create a program using some functions that I chose in order to benchmark them. I've added the program I've written to the *tests* folder and named it "*bench.c*". I modified the Makefile for it to compile that code too, so that a make at the root of the repo would suffice to compile everything.

I went for medium to big sized matrices in order to better see which parts of the code and which functions could be problematic. I decided to focus on the easiest functions, for instance *substract()*, *add()*, *print()* and *multiply()*. I'll also take a look at the *constructor()* and *destroy_matrix()* functions.
### Tools
I've decided to mainly go with **perf**, in addition to the **valgrind** suite of tools if deemed necessary. I'll also be using **time** to observe the evolution of execution time between modifications.
### Result
Here are the results I've had time wise :  
![pic](pic/base_time.png)  


I'll now show the the heaviest functions time-wise, to see if it changes after optimization :  
 ![pic](pic/base_perf_cpu.png)  

As we can see, the biggest "issue" comes from the multiplication, more precisely the *vector_multiply()* function, which is called by *multiply()* in our benchmarking program.  

Also notable, the *print()* function generates alot of small function calls, but these will be hard to reduce as the values have to be printed whatsoever.  

Cache wise, most of the misses come from the *vector_multiply()*, which isn't really surprising given that it's the most time consuming function.  

These observations lead me into trying to optimize the multiplication first, and to see if any change could then be reapplied to the other functions I used in the benchmarking program.
## 3
### Benchmark analysis
Please refer to the chapter **2 - Result**, which contains the analysis with the results I've had.

Now I'll just briefly talk about how the code is designed in its original form. The *Matrix* structure has 3 components, two *int*, row and column, which are quite self explanatory, and a *double *** called numbers. It's double pointer which will represent the numbers contained in the matrix. Each of the pointers will point to a column, meaning it's sort of a *column-major* implementation. This implementation means that we'll have as much "vectors" as there are columns, each containing "number of rows" elements. This means that the more columns we have, the more different vectors located in separate memory locations we'll have, which doesn't help with spatial locality, especially when working on a row.
### Optimization Idea
All my optimization ideas are directed at improving the performance speed wise.
#### Compiler Optimization
The first way to make sure the code benefits from a great deal of life changing (more like performance changing in this case) optimizations is to add the -O3 option to the compiler. As it was not used in the original Makefile, I made sure to add it, although I won't use it for the benchmarking, as I wouldn't see if my functions perform better otherwise.
#### First optimization
The results I've seen lead me to think about optimizing the function memory-wise, to allow faster access to the data in the matrices. The first improvement I've thought of is to replace the *double*** parameter in the *Matrix* struct with a simple *double** and to store all the data sequentially in memory. I've decided to store the data row by row. This will require to change the functions I'm aiming to optimize. To do so without breaking everything, I'll create duplicates of everything I change (functions, struct,...) and add an *_optX* affix to them, where X is the optimization step. I'll update the *bench.c* file accordingly.
#### Second optimization
Another possibility to improve the code would be to parallelize it or use SIMD instructions. As we have seen, it's the multiplication that's time consuming, and using parallelism on it could be great. I've decided to go with parallelism for simplicity of implementation, using the OpenMP library.


## 4
### First optimization
This optimization is pretty self explanatory and reading the code should suffice. The code is pretty much the same as before, the only difference being the way the data is stored.
### Second optimization
I've used OpenMP to distribute the multiplication between an optional number of threads. Code wise, I haven't gone into anything complicated. I just divide the multiplication between the number of threads I want and each of the threads will loop on it's given part of the matrix
## 5
### First optimization
This optimization didn't really do as well as I had hoped for. Here's the time it took for the same operations as the base code :  
![pic](pic/opt1_time.png)  

As we can see the time taken is pretty much the same, the problem still being cache misses. There are too much data, which makes it run at pretty much the same speed as the base version given that we don't avoid alot of cache misses.  

Here is a comparison with 1000x1000 matrices between the base multiplication and the first optimization multiplication :  
![pic](pic/1000x1000_comp_base.png)  
Base  

![pic](pic/1000x1000_comp_opt1.png)   
opt1  

As we can see, we have less misses, which explains the slight performance improvement, but it sadly isn't enough.
### Second optimization
In this case, we ca see a huge increase time wise :  
![pic](pic/opt2_time.png)   

The benchmarking programm's runtime is about 2-3 times faster than it originally was. In this case I've used 4 parallel threads, as going higher didn't seem to produce better results.  

Cache wise, there are not improvement between this optimization and the first one.

Adding the -O3 option to the compiler also reduces the runtime.


## 6
### Conclusion
In conclusion, what we can take from the results I've had is that when dealing with big data arrays, reducing cache-misses becomes quite tedious, whereas parallelism can be quite simple to implement and give a huge boost to performance.  

The downside is that parallelism quickly reaches it's limits (depends on the number of available CPUs), and when not sufficient, we'll have to deal with ways of improving the cache.  

As for this library itself, if most of it were to be made to use parallelism, it's performances would greatly increase. To go even further, we could try to optimize cache access even more and also make use of SIMD instructions.
