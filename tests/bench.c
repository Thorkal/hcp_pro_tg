#include "../yasML.h"

#define M_HEIGHT 2000
#define M_WIDTH 2000


int main(void){

    // Matrix *m1;
    // Matrix *m2;
    // Matrix *m3;
    // Matrix *m4;
    //
    // m1 = constructor(M_HEIGHT, M_WIDTH);
    // m2 = constructor(M_HEIGHT, M_WIDTH);
    //
    // add(m1, m2);
    //
    // subtract(m1, m2);
    //
    // m3 = multiply(m1, m2);
    //
    // equals(m1, m2);
    //
    // m4 = transpose(m3);
    //
    // print(m1);
    // print(m2);
    // print(m3);
    // print(m4);
    //
    // destroy_matrix(m1);
    // destroy_matrix(m2);
    // destroy_matrix(m3);
    // destroy_matrix(m4);


    Matrix_opt1 *m5;
    Matrix_opt1 *m6;
    Matrix_opt1 *m7;
    Matrix_opt1 *m8;


    m5 = constructor_opt1(M_HEIGHT, M_WIDTH);
    m6 = constructor_opt1(M_HEIGHT, M_WIDTH);


    add_opt1(m5, m6);


    subtract_opt1(m5, m6);


    m7 = multiply_opt1(m5, m6);


    equals_opt1(m5, m6);


    m8 = transpose_opt1(m7);


    print_opt1(m5);
    print_opt1(m6);
    print_opt1(m7);
    print_opt1(m8);


    destroy_matrix_opt1(m5);
    destroy_matrix_opt1(m6);
    destroy_matrix_opt1(m7);
    destroy_matrix_opt1(m8);

    return 0;
}
